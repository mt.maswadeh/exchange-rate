// To parse this JSON data, do
//
//     final rates = ratesFromJson(jsonString);

import 'dart:convert';

class Data {
  Data({
    required this.provider,
    required this.warningUpgradeToV6,
    required this.terms,
    required this.base,
    required this.date,
    required this.timeLastUpdated,
    required this.rates,
  });

  final String provider;
  final String warningUpgradeToV6;
  final String terms;
  final String base;
  final String date;
  final int timeLastUpdated;
  final Map<String, double> rates;

  factory Data.fromRawJson(String str) => Data.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        provider: json["provider"],
        warningUpgradeToV6: json["WARNING_UPGRADE_TO_V6"],
        terms: json["terms"],
        base: json["base"],
        date: json["date"],
        timeLastUpdated: json["time_last_updated"],
        rates: Map.from(json["rates"])
            .map((k, v) => MapEntry<String, double>(k, v.toDouble())),
      );

  Map<String, dynamic> toJson() => {
        "provider": provider,
        "WARNING_UPGRADE_TO_V6": warningUpgradeToV6,
        "terms": terms,
        "base": base,
        "date": date,
        "time_last_updated": timeLastUpdated,
        "rates": Map.from(rates).map((k, v) => MapEntry<String, dynamic>(k, v)),
      };
}
