class Rate {
  final String provider;
  final String warning;

  const Rate({
    required this.provider,
    required this.warning,
  });

  factory Rate.fromJson(Map<String, dynamic> json) {
    return Rate(
      provider: json['provider'],
      warning: json['WARNING_UPGRADE_TO_V6'],
    );
  }
}
