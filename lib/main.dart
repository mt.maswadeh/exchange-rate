import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'data.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Assignment'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<Data> ratesData;

  @override
  void initState() {
    super.initState();
    ratesData = fetchRates();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            headerSection,
            Container(
              margin: const EdgeInsets.all(8),
              child: FutureBuilder<Data>(
                future: ratesData,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var date = DateTime.fromMicrosecondsSinceEpoch(
                        snapshot.data!.timeLastUpdated);
                    final DateFormat dateFormatter = DateFormat('DD MMM, yyyy');
                    final String formattedLastUpdated =
                        dateFormatter.format(date);
                    return Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Base : ' + snapshot.data!.base),
                        Text('Date : ' + snapshot.data!.date),
                        Text('Last Updated : ' + formattedLastUpdated),
                        SizedBox(
                          height: 500,
                          child: ListView.builder(
                              padding: const EdgeInsets.all(20),
                              itemCount: snapshot.data!.rates.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(snapshot.data!.rates.keys
                                        .elementAt(index)),
                                    const Spacer(flex: 5),
                                    Text(snapshot
                                        .data!
                                        .rates[snapshot.data!.rates.keys
                                            .elementAt(index)]
                                        .toString()),
                                  ],
                                );
                              }),
                        )
                      ],
                    );
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  return const CircularProgressIndicator();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<Data> fetchRates() async {
  final response = await http
      .get(Uri.parse('https://api.exchangerate-api.com/v4/latest/USD'));

  if (response.statusCode == 200) {
    return Data.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load rates');
  }
}

Widget headerSection = Container(
  margin: const EdgeInsets.all(8),
  child: const Center(
    child: Text(
      'Exchange Rate',
      style: TextStyle(
        fontSize: 16,
      ),
    ),
  ),
);
